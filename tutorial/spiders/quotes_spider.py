import scrapy


class QuotesSpider(scrapy.Spider):
    name = 'quotes'

    start_urls = [
        'http://quotes.toscrape.com/page/1/'
    ]

    def parse(self, response):
        for quote in response.xpath('//div[@class="quote"]'):
            yield {
                'text': quote.xpath('.//span[@class="text"]/text()').get(),
                'author': quote.xpath('.//small[@class="author"]/text()').get(),
                'tags': quote.xpath('.//div[@class="tags"]/a/text()').getall()

            }

        next_page = response.xpath('//li[@class="next"]/a/@href').get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)


class AuthorSpider(scrapy.Spider):
    name = 'author'

    start_urls = [
        'http://quotes.toscrape.com/page/1/'
    ]

    def parse(self, response):
        for href in response.xpath('//span/a[contains(text(),"about")]/@href'):
            yield response.follow(href, callback=self.parse_author)

        for href in response.xpath('//li[@class="next"]/a/@href'):
            yield response.follow(href, callback=self.parse)

    def parse_author(self, response):
        yield {
            'name': response.xpath('//h3[@class="author-title"]/text()').get().strip(),
            'birthdate': response.xpath('//span[@class="author-born-date"]/text()').get(),
            'bio': response.xpath('//div[@class="author-description"]/text()').get().strip()
        }


class FilteredQuotesSpider(scrapy.Spider):
    name = 'filtered_quotes'

    def start_requests(self):
        url = 'http://quotes.toscrape.com/'
        tag = getattr(self, 'tag', None)
        if tag is not None:
            url = url + 'tag/' + tag
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        for quote in response.xpath('//div[@class="quote"]'):
            yield {
                'text': quote.xpath('.//span[@class="text"]/text()').get(),
                'author': quote.xpath('.//small[@class="author"]/text()').get()
            }

        next_page = response.xpath('//li[@class="next"]/a/@href').get()
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
